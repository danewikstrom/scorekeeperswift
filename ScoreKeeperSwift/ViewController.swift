//
//  ViewController.swift
//  ScoreKeeperSwift
//
//  Created by Dane Wikstrom on 9/14/17.
//  Copyright © 2017 Dane Wikstrom. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate
{
    
    @IBOutlet weak var team1ScoreLabel: UILabel!
    @IBOutlet weak var team2ScoreLabel: UILabel!
    @IBOutlet weak var team1Stepper: UIStepper!
    @IBOutlet weak var team2Stepper: UIStepper!
    @IBOutlet weak var team1TextField: UITextField!
    @IBOutlet weak var team2TextField: UITextField!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        team1Stepper.maximumValue = 11
        team2Stepper.maximumValue = 11
        team1TextField.delegate = self
        team2TextField.delegate = self
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Called when user presses team1's stepper. Updates score label
    @IBAction func team1StepperChanged(_ sender: Any)
    {
        let team1Score = String(format: "%.0f", team1Stepper.value)
        team1ScoreLabel.text = team1Score
    }
    
    //Called when user presses team2's stepper. Updates score label
    @IBAction func team2StepperChanged(_ sender: Any)
    {
        let team2Score = String(format: "%.0f", team2Stepper.value)
        team2ScoreLabel.text = team2Score
    }
    
    //Called when the user presses the reset score button. Updates score label and stepper
    @IBAction func resetBtnTouched(_ sender: Any)
    {
        team1Stepper.value = 0
        team1ScoreLabel.text = "0"
        team2Stepper.value = 0
        team2ScoreLabel.text = "0"
    }
    
    //MARK: UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        // Hide the keyboard.
        textField.resignFirstResponder()
        return true
    }
}

